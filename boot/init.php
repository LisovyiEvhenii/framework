<?php
//echo phpinfo();
require_once '../boot/constants.php';
require_once BOOT . '/Boot.php';

const CLASS_DIRS = array(
    'base' => CODE . '/base',
    'custom' => CODE . '/custom',
);

function _autoloader($className)
{
    $pathToFile = str_replace('_', '/', $className) . '.php';
    foreach (CLASS_DIRS as $dir)
        if (is_file($file = $dir . '/' . $pathToFile)) {
            require_once $file;
            break;
        }
}

try {
    spl_autoload_register('_autoloader');
    Boot::init_system_logs();
    Boot::init_modules();
} catch(Exception $e){
    die($e->getMessage() . ' on line ' .$e->getLine() . ' in ' . $e->getFile() . "<br>Trace:<br>" .
        str_replace('#', '<br>#', $e->getTraceAsString()));
}
require_once BOOT . '/errorHandling.php';
if (isset($_SERVER['DEVMODE']) && $_SERVER['DEVMODE'] == 1) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
} else {
    set_error_handler('error_handler');
    register_shutdown_function('fatal_error_handler');
}
