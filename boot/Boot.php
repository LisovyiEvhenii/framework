<?php

final class Boot
{
    public static $modules = array();

    public static function log($message, $fromFile = 'UNKNOWN', $line = '-1', $file = 'system.log')
    {
        $fileName = SYSTEM_LOG . '/' . $file;
        if (!is_file($fileName)) {
            touch($fileName);
            chmod($fileName, 0777);
        }
        $stream = fopen($fileName, 'a');
        $date = getdate();
        fwrite($stream, "Boot::log() at {$date['mday']}.{$date['mon']}.{$date['year']} {$date['hours']}:{$date['minutes']}:{$date['seconds']}" . "} from $fromFile on $line line\n");
        fwrite($stream, print_r($message, true));
    }

    public static function init_system_logs()
    {
        if (!is_dir(PATH_TO_VAR) && !mkdir(PATH_TO_VAR, 0777) && !chmod(PATH_TO_VAR, 0777))
            throw new Exception('Permissions to create ' . PATH_TO_VAR . ' DENIED');
        if (!is_dir(SYSTEM_LOG) && !mkdir(SYSTEM_LOG, 0777) && !chmod(SYSTEM_LOG, 0777))
            throw new Exception('Permissions to create ' . SYSTEM_LOG . ' DENIED');
    }

    public static function init_modules()
    {
        //check all base modules and set it to array $modules_dirs
        //$modules_dirs[] = 'dir_name'
        //$modules['name'] = 'path'
        foreach (CLASS_DIRS as $class_dir) {
            if (is_dir($class_dir)) {
                $dirs = scandir($class_dir);
                unset($dirs[0]);
                unset($dirs[1]);
                foreach ($dirs as $dir) {
                    $config = self::getStatusConfig($class_dir . '/' .$dir);
                }
            } else {
                throw new Exception('Constant CLASS_DIRS contain path to unknown directory ' . $dir);
            }
        }
    }

    public static function getStatusConfig($dir){
        $filename = $dir . MODULE_STATUS;
        $XMLElement = simplexml_load_file($filename);
        return $XMLElement;
    }
}