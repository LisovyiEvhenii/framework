<?php
define('ROOT', dirname(__DIR__));
define('PATH_TO_VAR', ROOT . '/var');
define('SYSTEM_LOG', ROOT . '/var/logs');
define('BOOT', ROOT . '/boot');
define('CODE', ROOT . '/code');
define('MODULE_STATUS', '/config/status.xml');