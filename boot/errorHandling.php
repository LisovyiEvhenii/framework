<?php

function error_handler($errno, $errstr, $errfile, $errline)
{
    Boot::log(array(
        'error' => $errstr,
        'error_number' => $errno,
        'line' => $errline,
        'file' => $errfile
    ));
}

function fatal_error_handler()
{
    if ($f_error = error_get_last()) {
        Boot::log(array(
            'error' => $f_error['message'],
            'line' => $f_error['line'],
            'file' => $f_error['file']
        ));
    }
}

